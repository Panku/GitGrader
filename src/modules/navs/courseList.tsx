import { 
  Button, 
  Dialog, 
  DialogActions, 
  DialogContent, 
  DialogContentText, 
  DialogTitle, 
  Grid, 
  Link, 
  makeStyles,
} from '@material-ui/core';
import * as React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { ICanvasNamespace } from '../../api/interfaces';
import { CourseCard } from './courseCard';
import { inject, observer } from 'mobx-react';
import relationStore from '../../stores/RelationStore';
import { AddCard } from './addCard';

const useStyles = makeStyles({
  card: {
    margin: '20px',
    maxWidth: '21%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  body: {
    width: '100%',
    paddingLeft: '5vw',
    paddingRight: '5vw',
  },
  centerItem: {
    width: '100%',
    textAlign: 'center'
  },
  actionButton: {
    color: 'white'
  } 
});

export const CourseList = 
inject('RelationStore')
(observer(() => {
  const classes = useStyles();
  const noSettings = (localStorage.getItem('CHdata') === null || localStorage.getItem('GHdata') === null ||
                      localStorage.getItem('CTdata') === null || localStorage.getItem('GTdata') === null ||
                      localStorage.getItem('CHdata') === '' || localStorage.getItem('GHdata') === '' ||
                      localStorage.getItem('CTdata') === '' || localStorage.getItem('GTdata') === '') 
                      ? true : false;

  return (
    <>
      <Grid
        container
        className={classes.body}
        justify='center'
        alignItems='center'
      >
        <div className={classes.centerItem}>
          <h2>Canvas / Gitlab relationships</h2>
        </div>
        {relationStore.relations.map((course: ICanvasNamespace) => (
          <Grid item xs={3} key={course.id} className={classes.card}>
            <CourseCard course={course} />
          </Grid>
        ))}
        <Grid item xs={3} className={classes.card}>
          <Link component={RouterLink} to={`/add`}>
            <AddCard />
          </Link>
        </Grid>
      </Grid>
      <Dialog open={noSettings}>
        <DialogTitle>{'Missing Settings'}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            One or more host URLs and/or access tokens are missing or have yet
            to be setup. Please go to the settings page to fix this issue.
          </DialogContentText>
          <DialogActions>
            <Button variant='outlined' color='primary'>
              <Link component={RouterLink} to='/settings'>
                Settings
              </Link>
            </Button>
          </DialogActions>
        </DialogContent>
      </Dialog>
    </> 
  );
}));
