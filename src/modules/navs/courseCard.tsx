import {
  Card,
  CardActionArea,
  CardContent,
  Link,
  makeStyles,
  Paper,
  Typography
} from '@material-ui/core';
import PersonIcon from '@material-ui/icons/Person';
import * as React from 'react';
import styled from 'styled-components';
import { ICanvasNamespace } from '../../api/interfaces';
import { Link as RouterLink } from 'react-router-dom';
import DeleteIcon from '@material-ui/icons/Delete';
import Popover from '@material-ui/core/Popover';
import Button from '@material-ui/core/Button';
import relationStore from '../../stores/RelationStore';

const useStyles = makeStyles({
  root: {
    width: 300,
    height: 320
  },
  card: {
    width: '300px',
    height: '270px'
  },
  media: {
    height: 140
  },
  icon: {
    color: '#424242',
    margin: '12px 12px',
    fontSize: 25,
  },
  popover: {
    padding: '15px',
    p: {
      paddingTop: '5px'
    }
  }
});

const colors = [
  { l: '#D38312', r: '#A83279' },
  { l: '#70E1F5', r: '#FFD194' },
  { l: '#9D50BB', r: '#6E48AA' },
  { l: '#B3FFAB', r: '#12FFF7' },
  { l: '#AAFFA9', r: '#11FFBD' },
  { l: '#FBD3E9', r: '#BB377D' },
  { l: '#C9FFBF', r: '#C9FFBF' },
  { l: '#B993D6', r: '#8CA6DB' },
  { l: '#00d2ff', r: '#3a7bd5' },
  { l: '#33ccff', r: '#ff99cc' },
  { l: '#ff758c', r: '#ff7eb3' }
];

interface IProps {
  colors: {
    l: string;
    r: string;
  };
}

const ImagePlaceholder = styled.div<IProps>`
  background-image: linear-gradient(-70deg, ${p => p.colors.l}, ${p => p.colors.r});
  button {
    color: inherit;
    text-decoration: none;
  }
`;

export const CourseCard = (props: { course: ICanvasNamespace }) => {
  const color = colors[Number(props.course.id) % 11];
  const { course } = props;
  const classes = useStyles();
  const [anchor, setAnchor] = React.useState<any>(null);

  const open = Boolean(anchor);
  const id = open ? 'simple-popover' : undefined;

  return (
      <Card className={classes.card}>
        <ImagePlaceholder colors={color}>
          <Button style={{float: 'right'}}>
            <DeleteIcon className={classes.icon} onClick={(e) => setAnchor(e.currentTarget)}/>
          </Button>
          <Popover
            id={id}
            open={open}
            anchorEl={anchor}
            onClose={() => setAnchor(null)}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
          >
            <div className={classes.popover}>
              <Typography color='textSecondary'>Remove course-gitlab relationship?</Typography>
              <Button
                onClick={() => {
                  relationStore.delete(course.id);
                }}
              >
                Confirm
              </Button>
              <Button onClick={() => setAnchor(null)}>
                Cancel
              </Button>
            </div>
          </Popover>
          <div style={{display: 'inline-flex', paddingLeft: '80%', paddingTop: '15%', color: '#424242'}}>
            <PersonIcon/>
            {props.course.total_students}
          </div>
        </ImagePlaceholder>
        <CardActionArea>
          <Link component={RouterLink} to={`/course/${course.id}`}>
            <CardContent>
              <Typography variant='body1' color='textPrimary'>{props.course.name}</Typography>
              <br/><br/>
              <div style={{display: 'inline-flex', width: '100%'}}>
                <Typography color='textSecondary'>
                  {props.course.teachers[0].display_name}
                </Typography>
              </div>
            </CardContent>
          </Link>
        </CardActionArea>
      </Card>
  );
};
