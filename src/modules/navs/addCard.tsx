import {
  Card,
  CardActionArea,
  CardContent,
  makeStyles,
  Typography
} from '@material-ui/core';
import * as React from 'react';
import styled from 'styled-components';

const useStyles = makeStyles({
  root: {
    width: 280,
    height: 310
  },
  card: {
    width: '300px',
    height: '270px'
  }
});

interface IProps {
  colors: {
    l: string;
    r: string;
  };
}

const ImagePlaceholder = styled.div<IProps>`
  background-color: #424242;
  width: 100%;
  height: 140px;
  text-align: center;
  span {
    font-size: 130px;
  }
  color: white;
`;

export const AddCard = () => {
  const color = { l: '#667eea', r: '#764ba2' };
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <CardActionArea>
        <ImagePlaceholder colors={color}>
          <span>&#43;</span>
        </ImagePlaceholder>
        <CardContent style={{paddingBottom: '60px'}}>
          <Typography variant='h6'>Add relationship</Typography>
          <Typography color='textSecondary'>
            Canvas / Git Group
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};