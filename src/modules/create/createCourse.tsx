import { Button, FormControl, Grid, InputLabel, MenuItem, Select, TextField, withStyles } from '@material-ui/core';
import LinkIcon from '@material-ui/icons/Link';
import * as React from 'react';
import { ICanvasClass, IGitNamespace } from '../../api/interfaces';
import {CanvasAPI, GitLabAPI} from '../../app';
import relationStore from '../../stores/RelationStore';
import { useHistory } from 'react-router-dom';

const MainGrid = withStyles({
  root: {
    position: 'absolute',
    top: '400px'
  }
})(Grid);

export const CreateCourse = () => {
  const [courses, setCourses] = React.useState<ICanvasClass[]>();
  const [namespaces, setNamespaces] = React.useState<IGitNamespace[]>();

  const [selectedNamespace, setSelectedNamespace] = React.useState('');
  const [selectedCourse, setSelectedCourse] = React.useState('');
  const [section, setSection] = React.useState('');
  const history = useHistory();

  const handleChangeNamespace = (event: any) => {
    setSelectedNamespace(event.target.value);
  }; 
  
  const handleChangeCourse = (event: any) => {
    setSelectedCourse(event.target.value);
  };

  const handleSubmit = () => {
    if(!courses || !namespaces) {
      return console.log(`Course or namespace missing.`);
    }
    const course = courses.find(c => c.id == selectedCourse);
    const ns = namespaces.find(n => n.id == selectedNamespace);

    if(!course || !ns) {
      return console.log(`Couldn't find course or ns`);
    }
    history.goBack();
    relationStore.add(course, section, ns);
  };
  
  React.useEffect(() => {
    CanvasAPI.getClasses()
      .then(courses => {
        setCourses(courses);
      })
      .catch(console.error);
  }, [CanvasAPI]);

  React.useEffect(() => {
    GitLabAPI.getNamespaces()
      .then(namespaces => {
        setNamespaces(namespaces);
      })
      .catch(console.error);
  }, [GitLabAPI]);

  const disabled = Boolean(selectedCourse === '' || selectedNamespace === '' || section === '');

  return (
    <MainGrid
      container
      direction='column'
      justify='center'
      alignItems='center'
      style={{width: '100%'}}
      spacing={3}
    >
      <Grid
        container
        direction='row'
        justify='center'
        spacing={3}
      >
        <Grid item>
          <FormControl style={{width: 200}}>
            <InputLabel id='canvasIDlabel'></InputLabel>
            <Select
              labelWidth={20}
              displayEmpty
              onChange={handleChangeCourse}
              value={selectedCourse}
              inputProps={{
                id: 'canvasID'
              }}
            >
              <MenuItem value='' disabled>
                Select a course
              </MenuItem>
              {
                courses ? courses.map(course => (
                  <MenuItem key={course.id} value={course.id}>
                    {course.name}
                  </MenuItem>
                )) :
                  <MenuItem value='' disabled>
                    No courses found
                  </MenuItem>
              }
            </Select>
          </FormControl>
        </Grid>
        <Grid item style={{margin: '20px'}}>
          <LinkIcon fontSize='default'/>
        </Grid>
        <Grid item>
          <FormControl style={{ width: 200 }}>
            <InputLabel id='githubIDlabel'></InputLabel>
            <Select
              labelWidth={20}
              displayEmpty
              onChange={handleChangeNamespace}
              value={selectedNamespace}
              inputProps={{
                id: 'gitlabID'
              }}
            >
              <MenuItem value='' disabled>
                Select a group
              </MenuItem>
              {
                namespaces ? namespaces.map(namespace => (
                  <MenuItem key={namespace.id} value={namespace.id}>
                    {namespace.name}
                  </MenuItem>
                )) :
                  <MenuItem value='' disabled>
                    No namespaces found
                  </MenuItem>
              }
            </Select>
          </FormControl>
        </Grid>
      </Grid>
      <Grid item>
        <TextField
          type='text'
          label='Class Section'
          onChange={e => setSection(e.target.value)}
        />
      </Grid>
      <Grid item>
        <FormControl>
          <Button onClick={handleSubmit} disabled={disabled} type='submit'>
            Create
          </Button>
        </FormControl>
      </Grid>
    </MainGrid>
  );
};