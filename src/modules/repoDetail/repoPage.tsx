import {
  Button, 
  Card,
  CardContent,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  makeStyles, 
  Tooltip, 
  Typography,
  DialogContentText,
  CardHeader,
  List,
  ListItem,
  ListItemText
} from '@material-ui/core';
import * as React from 'react';
import styled from 'styled-components';
import { ICanvasNamespace } from '../../api/interfaces';
import { inject, observer } from 'mobx-react';
import BaseRepo from '../../stores/BaseRepo';
import baseRepoStore from '../../stores/BaseRepoStore';
import { GitLabAPI } from '../../app';
import { useHistory } from 'react-router-dom';

const SpacePadding = styled.div`
  margin-bottom: 20px;
`;

const useStyles = makeStyles({
  content: {
    height: '100vh',    
    width: '100vw'
  },
  header: {
    height: '15vh',
    display: 'flex',
    justifyContent: 'space-evenly',
    alignItems: 'center'
  },
  body: {
    display: 'flex',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  files: {
    width: '400px',
    height: '500px'
  },
  studentHeader: {
    display: 'flex',
    justifyContent: 'space-evenly',
    borderBottom: '2px solid #303030',
    fontSize: '1.5rem',
    padding: '15px'
  },
  students: {
    width: '50vw',
    height: '500px'
  },
  repos: {
    display: 'flex',
    flexDirection: 'row'
  },
  repoList: {
    width: '100%',
    height: '400px',
    marginTop: '5px',
    overflowY: 'auto',
  },
  repoWithOut: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  repoButton: {
    backgroundColor: '#303030',
    border: 0,
    borderRadius: 3,
    color: 'white',
    height: 32,
    padding: '0 30px'
  },
  actionCard: {
    width: '200px',
    height: '500px',
  },
  actionHeader: {
    borderBottom: '2px solid #303030',
    textAlign: 'center'
  },
  listButton: {
    textAlign: 'center'
  },
  actionButton: {
    color: 'white',
    margin: '7px'
  },
  '@global': {
    '*::-webkit-scrollbar': {
      width: '0.5em'
    },
    '*::-webkit-scrollbar-track': {
      '-webkit-box-shadow': 'inset 0 0 6px rgba(0,0,0,0.00)'
    },
    '*::-webkit-scrollbar-thumb': {
      backgroundColor: 'rgba(0,0,0,.1)',
      outline: '2px solid slategrey'
    }
  }
});

function sleep(time: number) {
  return new Promise(res => setTimeout(res, time));
}

export const RepoPage = 
  inject('BaseRepoStore')
  (observer((props: {
    repo: BaseRepo, 
    course: ICanvasNamespace,
    semester: string 
}) => {
  const history = useHistory();
  const classes = useStyles();
  const { repo, course, semester } = props;
  const [filesNames, setFileNames] = React.useState([]);
  const [uploadConf, setUploadConf] = React.useState(false);
  const [editConf, setEditConf] = React.useState(false);
  const [deleteCheck, setDeleteCheck] = React.useState(false);
  const [uploadFiles, setUpload] = React.useState<string[]>();
  const [updateFiles, setUpdate] = React.useState<string[]>();
  const [nuke, setNuke] = React.useState(false);
  const [stuNuke, setStudentNuke] = React.useState(false);
  const year = new Date().getFullYear();
  
  const withRepos = [];
  const withoutRepos = [];

  for(const [username, git] of repo.username_to_git_id) {
    if(repo.user_to_ass_id.get(git)) {
      withRepos.push(username);
    } else {
      withoutRepos.push(username);
    }
  }

  React.useEffect(() => {
    GitLabAPI.listFiles(repo.id)
      .then(() => {
        const localFiles = JSON.parse(localStorage.getItem('filesList') || 'null');
        const names = localFiles[repo.id] ? localFiles[repo.id].names : [];

        setFileNames(names);
      })
      .catch(console.error);
  }, []);

  const upload = (actions: Array<{
    action: string, file_path: string, content: string, encoding: string
    }>) => {
    GitLabAPI.uploadFile(repo.id, actions)
      .then(() => console.log(`Uploaded file(s)`))
      .catch(console.error);
  };

  const edit = (actions: Array<{
    action: string, file_path: string, content: string, encoding: string
    }>) => {
    GitLabAPI.editFile(repo.id, actions)
      .then(() => console.log(`Updated file(s)`))
      .catch(console.error);
  };

  const convertBase64Upload = () => {
    const file = document.getElementById('file-upload') as HTMLInputElement;
    const actionsArray: {
      action: string, file_path: string, content: string, encoding: string
      }[] = [];
    
    if(file.files) {
      for(let i = 0; i < file.files.length; i++) {
        if(file.files.item(i)) { 
          const file_item = file.files.item(i) as File;
          actionsArray.push(readerSetup(file_item, 'create'));
        }
      }
    }

    setTimeout(() => {
      console.log(actionsArray);
      upload(actionsArray);
    }, 2000);

    setUploadConf(false);
  };

  const convertBase64Edit = () => {
    const file = document.getElementById('file-edit') as HTMLInputElement;
    const actionsArray: {
      action: string, file_path: string, content: string, encoding: string
      }[] = [];
    
    if(file.files && updateFiles) {
      const files = Array.from(file.files)
        .filter(f => updateFiles.indexOf(f.name) !== -1);

      for(const f of files) {
        actionsArray.push(readerSetup(f, 'update'));
      }
    }

    setTimeout(() => {
      console.log(actionsArray);
      edit(actionsArray);
    }, 2000);

    setEditConf(false);
  };

  const readerSetup = (file: File, choice: string) => {
    const action = {
      action: `${choice}`,
      file_path: '',
      content: '',
      encoding: 'base64'
    };

    if(file) {
      const reader = new FileReader();
      const file_name = file.name;
      let base64content = 'VXBsb2FkIGVycm9y';
      
      reader.onloadend = () => {
        base64content = reader.result as string;
        base64content = base64content.split(',')[1];
        action.file_path = file_name;
        action.content = base64content;
      };
      
      if(file) {
        reader.readAsDataURL(file);
      }
    }
    
    return action;
  };

  const listUploadFiles = () => {
    
    const file = document.getElementById('file-upload') as HTMLInputElement;
    
    if(file.files) {
      setUploadConf(true);
      setUpload(
        Array.from(file.files)
          .map(f => f.name)
      );
    }
  };

  const listUpdateFiles = () => {
    
    const file = document.getElementById('file-edit') as HTMLInputElement;
    const files = JSON.parse(localStorage.getItem('filesList') || 'null');    
    const names: string[] = files[repo.id] ? files[repo.id].names : [];

    if(file.files) {
      const files_in_repo = Array.from(file.files)
        .filter(f => names.indexOf(f.name) !== -1)
        .map(f => f.name);

      if(files_in_repo.length) {
        setEditConf(true);
        setUpdate(files_in_repo);
      }
    }
  };

  /**
   * @TODO - Add toasties for success / errors.
   */
  const remove = () => {
    baseRepoStore.delete(repo);
    setDeleteCheck(false);
  };

  const nukeAll = () => {
    baseRepoStore.nuke(repo)
      .then(() => history.goBack())
      .catch(console.error);
    setNuke(false);
  };

  const studentNuke = () => {
    baseRepoStore.nuke(repo, true)
      .catch(console.error);
    setStudentNuke(false);
  };

  const handleNuke = () => {
    setNuke(false);
  };

  return (
    <div className={classes.content}>
      <div className={classes.header}>
        <Typography variant='h2' color='textPrimary'>{repo.name}</Typography>
      </div>
      <div className={classes.body}>
        <Card className={classes.actionCard}>
          <CardContent>
            <CardHeader className={classes.actionHeader} title='Actions' />
            <List>
              <ListItem 
                button 
                className={classes.listButton}
                onClick={async () => {
                  // If we DON'T await this, then there's a possibility of failing to create.
                  for(const u of Array.from(repo.username_to_git_id.keys())) {
                    await sleep(700);
                    await repo.createAssignment(course.section, `${year}-${semester.toUpperCase()}`, u);
                  }
                }}
              >
                <ListItemText primary='CREATE ALL' />
              </ListItem>
              <ListItem 
                button 
                className={classes.listButton}
                onClick={() => {
                  // Currently assigns all students.
                  repo.assign()
                    .catch(console.error);
                }}
              >
                <ListItemText primary='ASSIGN' />
              </ListItem>
              <ListItem 
                button 
                className={classes.listButton}
                onClick={() => {
                  for(const u of Array.from(repo.username_to_git_id.values())) {
                    repo.unlock(u);
                  }
                }}
              >
                <ListItemText primary='UNLOCK' />
              </ListItem>
              <ListItem 
                button 
                className={classes.listButton}
                onClick={() => {
                  for(const u of Array.from(repo.username_to_git_id.values())) {
                    repo.lock(u);
                  }
                }}
              >
                <ListItemText primary='LOCK' />
              </ListItem>
              <ListItem 
                button 
                className={classes.listButton}
                onClick={() => {
                  repo.archive();
                }} 
              >
                <ListItemText primary='ARCHIVE' />
              </ListItem>

              <Tooltip title='Delete the base repo' placement='top'>
                <ListItem 
                  button 
                  className={classes.listButton}
                  onClick={()=> {
                    setDeleteCheck(true);
                    
                  }}
                >
                  <ListItemText primary='DELETE BASE' />
                </ListItem>
              </Tooltip>
              <Tooltip title='Delete all student repos' placement='top'>
                <ListItem 
                  button 
                  className={classes.listButton}
                  onClick={()=> {
                    setStudentNuke(true);
                  }}
                >
                  <ListItemText primary='DELETE STUDENTS' />
                </ListItem>
              </Tooltip>
              <Tooltip title='Delete the base and all student repos' placement='top'>
                <ListItem 
                  button 
                  className={classes.listButton}
                  onClick={()=> {
                    setNuke(true);
                  }}
                >
                  <ListItemText primary='DELETE ALL' />
                </ListItem>
              </Tooltip>
            </List>
          </CardContent>
        </Card>
        <Dialog
          open={deleteCheck}
          onClose={() => {
            setDeleteCheck(false);
          }}
        >
          <DialogContent>
            <DialogContentText>
              Are you sure you want to delete this repo?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => {
              setDeleteCheck(false);
            }} color='primary'>
              Cancel
            </Button>
            <Button onClick={remove} color='primary'>
              Delete
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          open={nuke}
          onClose={() => {
            setNuke(false);
          }}
        >
          <DialogContent>
            <DialogContentText>
              Are you sure you want to remove this and all student repos?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleNuke} color='primary'>
              Cancel
            </Button>
            <Button onClick={nukeAll} color='primary'>
              Delete
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          open={stuNuke}
          onClose={() => {
            setStudentNuke(false);
          }}
        >
          <DialogContent>
            <DialogContentText>
              Are you sure you want to remove all student repos?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => setStudentNuke(false)} color='primary'>
              Cancel
            </Button>
            <Button onClick={studentNuke} color='primary'>
              Delete
            </Button>
          </DialogActions>
        </Dialog>
        <Card className={classes.files}>
          <CardContent>
            <CardHeader className={classes.actionHeader} title='Files' />
            <br/>
            {
              filesNames.length ? 
              filesNames.map(f => (
                <div key={`${f}-div`}>
                  <Typography color='textSecondary' variant='subtitle1' key={f}>{f}</Typography>
                  <br/>
                </div>
              )) :
              <Typography variant='subtitle2'>No files in this repo.</Typography>
            }
            <SpacePadding></SpacePadding>
            <form>
              <input type='file' id='file-upload' multiple style={{display: 'none'}}/>
              <label htmlFor='file-upload'>
                <Button variant='contained' color='primary' component='span'>
                  Choose files
                </Button>
              </label>
              <Button className={classes.actionButton} onClick={listUploadFiles} variant='outlined' color='primary'> 
                <Typography color='textSecondary'>Upload</Typography>
              </Button>
              <br/>
              <input type='file' id='file-edit' multiple style={{display: 'none'}}/>
              <label htmlFor='file-edit'>
                <Button variant='contained' color='primary' component='span'>
                  Choose files
                </Button>
              </label>
              <Button 
                className={classes.actionButton} 
                onClick={listUpdateFiles} 
                variant='outlined' 
                color='primary'
              >
                <Typography color='textSecondary'>Update</Typography>
              </Button>
              <Dialog open={uploadConf} onClose={() => setUploadConf(false)}>
                <DialogTitle>{'Are you sure you want to upload these files?'}</DialogTitle>
                <DialogContent dividers>
                    {
                      uploadFiles ? 
                      uploadFiles.map(f => (<Typography variant='subtitle2' key={f}>{f}</Typography>)) :
                      <div>No files to upload.</div>
                    }
                </DialogContent>
                <DialogContent dividers>
                  <Button 
                    className={classes.actionButton} 
                    onClick={convertBase64Upload} 
                    variant='outlined' 
                    color='primary'
                  > 
                    Yes
                  </Button>
                  &nbsp;&nbsp;
                  <Button 
                    onClick={() => setUploadConf(false)}
                    variant='outlined' 
                    color='secondary'
                  >
                    No
                  </Button>
                </DialogContent>
              </Dialog>
            </form>
            <SpacePadding></SpacePadding>
            <form>
              <Dialog open={editConf} onClose={() => setEditConf(false)}>
                <DialogTitle>{'Are you sure you want to update these files?'}</DialogTitle>
                <DialogContent dividers>
                    {
                      updateFiles ? 
                      updateFiles.map(f => (<Typography variant='subtitle2' key={f}>{f}</Typography>)) :
                      <div>No files to update.</div>
                    }
                </DialogContent>
                <DialogContent dividers>
                  <Button 
                    className={classes.actionButton} 
                    onClick={convertBase64Edit} 
                    variant='outlined' 
                    color='primary'
                  > 
                    Yes
                  </Button>
                  &nbsp;&nbsp;
                  <Button 
                    onClick={() => setEditConf(false)}
                    variant='outlined' 
                    color='secondary'
                  >
                    No
                  </Button>
                </DialogContent>
              </Dialog>
            </form>
          </CardContent>
        </Card>
        <Card className={classes.students}>
          <CardContent>
            <div className={classes.studentHeader}>
              <div>With repos</div>
              <div>Without repos</div>
            </div>
            <div className={classes.repos} >
              <CardContent className={classes.repoList} style={{borderRight: '2px solid #303030'}}>
                {
                  withRepos.length ? withRepos.map(r => (<ul key={r}>{r}</ul>))
                    :
                  <span>No student repos. Create them!</span>
                }
              </CardContent>
              <CardContent className={classes.repoList}>
                {
                  withoutRepos.length ? withoutRepos.map(r => (
                    <div className={classes.repoWithOut}>
                      <ul key={r}>{r}</ul>
                      <Button 
                        className={classes.repoButton} 
                        key={`${r}-btn`} 
                        onClick={() => {
                          repo.createAssignment(course.section, `${year}-${semester.toUpperCase()}`, r)
                            .catch(console.error);
                      }}
                      >
                        Create
                      </Button>
                    </div>
                  ))
                    :
                  <span>Students have their repos... hopefully.</span>
                }
              </CardContent>
            </div>
          </CardContent>
        </Card>
      </div>
    </div>
  );
}));