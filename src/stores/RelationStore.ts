import { observable, IObservableArray } from 'mobx';
import { ICanvasNamespace, ICanvasClass, IGitNamespace } from '../api/interfaces';
import { GitLabAPI } from '../app';
import baseRepoStore from './BaseRepoStore';

export class RelationStore {
  @observable relations: IObservableArray<ICanvasNamespace> = observable.array();

  loadData = async (classes: ICanvasClass[]) => {
    const storedRelations = JSON.parse(localStorage.getItem('relations') || 'null');
    const namespaces = await GitLabAPI.getNamespaces();

    for(const c of classes) {
      if(storedRelations[c.id]) {
        const info = storedRelations[c.id];
        const n = namespaces.find(n => n.id == info.gitlabID);
        // IF the namespace isn't there.. then ignore.
        if (!n) {
          continue;
        }
      
        this.relations.push({
          ...c,
          section: info.section,
          namespace: {
            id: n.id,
            name: n.name,
            path: n.path
          }
        });
      }
    }
  }

  add = (
    course: ICanvasClass,
    section: string, 
    ns: IGitNamespace
  ) => {
    const rel = this.get(course.id);

    if(rel) {
      return;
    }

    const temp = JSON.parse(localStorage.getItem('relations') || '{}');
    temp[course.id] = {
      canvasID: course.id, 
      section: section, 
      gitlabID: ns.id
    };
    localStorage.setItem('relations', JSON.stringify(temp));
    this.relations.push({
      ...course,
      section: section,
      namespace: {
        id: ns.id,
        name: ns.name,
        path: ns.path
      } 
    });

    /**
     * Need to also get students usernames, assignments, git ids etc.
     * Basically implement the loadData on baseRepoStore. (: sigh
     * 
     * It just shows the base repos on the users side. If they try to assign students etc it'll fail to do so.
     */
    GitLabAPI.getRepos(ns.id)
      .then(repos => {
        baseRepoStore.add(ns.id, repos.base_repos);
      })
      .catch(console.error);
  }

  size = () => Array.from(this.relations.keys()).length;

  delete = (courseId: string) => {
    const rel = this.get(courseId);

    if(!rel) {
      return;
    }

    const temp = JSON.parse(localStorage.getItem('relations') || '{}');
    delete temp[courseId];
    localStorage.setItem('relations', JSON.stringify(temp));
    this.remove(courseId);  
  }

  all = () => Array.from(this.relations.values());

  remove = (courseId: string) => {
    const rel = this.get(courseId);

    if(!rel) {
      return;
    }

    this.relations.splice(this.relations.indexOf(rel), 1);
  }

  get = (courseId: string) => {
    return this.relations.find(r => r.id == courseId);
  }
}

export default new RelationStore();