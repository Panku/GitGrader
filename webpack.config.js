const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require("path");

module.exports = [
  {
    mode: 'development',
    entry: './src/app.tsx',
    target: 'web',
    module: {
      rules: [{
        test: /\.tsx?$/,
        include: /src/,
        use: [{ loader: 'ts-loader' }]
      }]
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'app.js'
    },
    node: {
      fs: 'empty',
      tls: 'empty'
    },

    plugins: [
      new HtmlWebpackPlugin({
        template: "./src/index.html"
      })
    ],

    resolve: {
      extensions: ['.ts', '.tsx', '.js']
    },
  }
];